﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManagerScript : MonoBehaviour
{
    public Text hitObj;   //HIT回数表示
    public Text blowObj;    //BLOW回数表示
    public Text countObj;   //回数表示
    public bool Newgame = false; //ニューゲーム
    public GameObject DecisionObj;//決定ボタン
    public GameObject Retry;//リトライボタン
    public GameObject Result;//正解

    int[] Answer = new int[3];
    int Duplicate;//重複確認

    public Dropdown DropNumber;
    public Dropdown DropNumber1;
    public Dropdown DropNumber2;

    public Text log;
    public Text log1;
    public Text log2;

    int[] inputNumber = new int[3];
    int blowdp;
    int hitcounter;

    int hit;
    int blow;

    // Start is called before the first frame update
    void Start()
    {

        hitObj.text = "0" + "HIT";
        blowObj.text = "0" + "BLOW";
        Result.SetActive(false);
        //答え作成
        AnswerGene();

    }

    // Update is called once per frame
    void Update()
    {
        Button button = DecisionObj.GetComponent<Button>();
        ////リプレイ初期化処理
        //if (Newgame == true)
        //{
        //    AnswerGene();
        //    button.count = 0;
        //    countObj.text = button.count + "回";
        //    hitObj.text = "0" + "HIT";
        //    blowObj.text = "0" + "BLOW";
        //    Result.SetActive(false);
        //    log2.text = "";
        //    log1.text = "";
        //    log.text = "";
        //    Newgame = false;
        //}

        //判定処理
        if (button.buttle == true)
        {
           
            inputNumber[0] = button.number / 100;//100の位1桁目
            inputNumber[1] = (button.number % 100) / 10;
            inputNumber[2] = (button.number % 100) % 10;
            Debug.Log("3桁目"+ inputNumber[2]);
            Debug.Log("2桁目" + inputNumber[1]);
            Debug.Log("1桁目" + inputNumber[0]);


            //    int[] DropDownNumber = { DropNumber.value, DropNumber1.value, DropNumber2.value };

            //    if (DropDownNumber[0] == Answer[0] && DropDownNumber[1] == Answer[1] && DropDownNumber[2] == Answer[2])
            if (Answer[0]==inputNumber[0]&& Answer[1] == inputNumber[1]&&Answer[2] == inputNumber[2])
                {
                    Debug.Log("正解");
                    Result.SetActive(true);
            //        Retry.SetActive(true);
                }
            //    if (Newgame == false)
            //    {
            hit = 0;
            blow = 0;
            hitObj.text = "0" + "HIT";
            blowObj.text = "0" + "BLOW";

            for (hitcounter = 0; hitcounter < 3; ++hitcounter)
            {
                if (Answer[hitcounter] == inputNumber[hitcounter])
                {
                    hit += 1;
                    Debug.Log(hit + "hit");
                    hitObj.text = hit + "HIT";
                    continue;
                }
                for (blowdp = 0; blowdp < 3; ++blowdp)
                {

                    if (Answer[hitcounter] == inputNumber[blowdp])
                    {
                        blow += 1;
                        Debug.Log(blow + "blow");
                        blowObj.text = blow + "BLOW";
                    }

                }

            }

                //    }

                //    log2.text = log1.text;
                //    log1.text = log.text;
                //    log.text = DropDownNumber[0] + "," + DropDownNumber[1] + "," + DropDownNumber[2];

                button.buttle = false;
            //    //解答回数カウント処理
            //    countObj.text = button.count + "回";
            }
        }


    //答え作成
    void AnswerGene()
        {
            Answer[0] = Random.Range(1, 10);
            Duplicate = Random.Range(1, 10);
            while (Answer[0] == Duplicate)
            {
                Duplicate = Random.Range(1, 10);
            }
            Answer[1] = Duplicate;
            Duplicate = Random.Range(1, 10);
            while (Answer[0] == Duplicate || Answer[1] == Duplicate)
            {
                Duplicate = Random.Range(1, 10);
            }
            Answer[2] = Duplicate;

            Debug.Log("答え" + Answer[0] + Answer[1] + Answer[2]);

            //Newgame = false;
        }
    
}

