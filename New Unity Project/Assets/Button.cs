﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    public InputField inputField;
    public Text text;//入力値
    public int count;
    public bool buttle = false;
    public int number;


    // ボタン押された
    public void OnClick()
    {
        //テキスト取得
        inputField = inputField.GetComponent<InputField>();
        text = text.GetComponent<Text>();
        //string型をint型に変換
        Int32.TryParse(inputField.text, out number);

        Debug.Log("入力値" + number);
        Debug.Log("ボタン押された");
        //count += 1;
        buttle = true;

    }

    public void OnClick2()
    {
        SceneManager.LoadScene("kazumittu");
    }

}