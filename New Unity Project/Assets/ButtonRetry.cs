﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonRetry : MonoBehaviour
{
    public GameObject Button;
    public GameObject ButtonRetryactive;
    public GameObject GM;
    public InputField inp;
    public Text colchan;

    public void OnClickRetry()
    {
        GameManagerScript GMSc = GM.GetComponent<GameManagerScript>();
        //決定ボタン表示
        Button.SetActive(true);
        //リプレイ
        GMSc.Newgame = true;
        //インプットフィールド初期化
        //inp = inp.GetComponent<InputField>();
        //inp.text = "";
        //色初期化
        //colchan.color = new Color(0.0f, 0.0f, 0.0f, 1.0f);
        //リプレイボタン非表示
        ButtonRetryactive.SetActive(false);
    }
}
